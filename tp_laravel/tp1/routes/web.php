<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProduitController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

route::get('/tp1', function () {
    return response('<h1>HELLO WORLD</h1>', 200)
        ->header('content-Type', 'text/html')
        ->header('foo', 'bar');
});

// route :: get('/posts/{id}', function($id){
//     return response('Post' . $id);
// })->where('id' , '[0-9]+');

route::get('/posts/{id}', function ($id) {
    dd($id);
    return response('Post' . $id);
})->where('id', '[0-9]+');

// route::get('/searsh', function ($request) {
//     dd($request);
// });

route::get('/searsh', function ($request) {
    return $request->name .''.$request->city;
});

Route::get('/searsh', function ($request) {
    return response();
});

//Q5

route ::get('/' , function(){
    $articles=[
        ["title"=>"produit 1" , "prix"=>200.5],
        ["title"=>"produit 2" , "prix"=>89],
        ["title"=>"produit 3" , "prix"=>78.5],

    ];
    return view('catalogue',["articles"=>$articles  ,"heading"=>"liste des produit"]);
});

Route::get('/',[ProduitController::class,'index']);