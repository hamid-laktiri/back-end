<?php

use App\Http\Controllers\Admin\AdminHomeController;
use App\Http\Controllers\Admin\AdminProductController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

    Route::get(('/'), [HomeController::class,"index"])->name("home.index");
    Route::get(('/about'), [HomeController::class,"about"])->name("home.about");
    route ::get(('/index'),[ProductController::class,"index"]) ->name("product.index");
    route :: get(('/product/{id}'),[ProductController::class,"show"])->name("product.show");
//     Route::get('/products','App\Http\Controllers\ProductController@index')->name("product.index");
// Route::get('/products/{id}','App\Http\Controllers\ProductController@show')->name("product.show");
route::get(('/admin'), [AdminHomeController::class, "index"])->name('admin.home.index');
route::get(('/admin/products'), [AdminProductController::class, "index"])->name('admin.product.index');
route::post(('/admin/products/store'),[AdminProductController::class, "store"])->name('admin.product.store');