@extends('layouts.app')
@section('title', $viewData["title"])
@section('content')
<div class="row">
<div class="col-md-6 col-lg-4 mb-2">
<img src="{{ asset('/img/game.png') }}" class="img-fluid rounded">
</div>
<div class="col-md-6 col-lg-4 mb-2">
<img src="{{ asset('/img/safe.png') }}" class="img-fluid rounded">
</div>
<div class="col-md-6 col-lg-4 mb-2">
<img src="{{ asset('/img/submarine.png') }}" class="img-fluid rounded">
</div>
</div>
@endsection
@section('ilyass')
bon pratique &#9400; <h6><b><i>Laktri_Iguidar</i></b> </h6>
@endsection