<?php
    function cleanInput($input)
    {
        $input = trim($input);
        $input = stripslashes($input);
        $input = htmlspecialchars($input);
        return $input;
    }
    if ($_SERVER["REQUEST_METHOD"]==="POST")
    {
        if (isset($_POST["supprimer"]))
        {
            $id = $_POST["id"];
            try {
            $pdo = require_once "connnection.php";
            $sql = 'DELETE FROM users WHERE id=:id;';
            $stm = $pdo->prepare($sql);
            $stm->bindParam(":id",$id);
            $stm->execute();
            $pdo=null;
            $msg = "L'utilisateur est supprime avec success";
            }
            catch (PDOException $err) {
                $msg=$err->getMessage();
            }
            catch (\Throwable $th) {
                //throw $th;
            }
            
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h4>Supprimer Un Utilisateur</h4>
    <div>
        <p><?=isset($msg)?$msg:""?></p>
    </div>
    <form action="<?=htmlspecialchars($_SERVER["PHP_SELF"])?>" method="POST">
        <label for="">id de l'utilisateur: </label>
        <input type="text" name="id"><br>
        <input type="submit" value="supprimer" name="supprimer">
    </form>
</body>
</html>