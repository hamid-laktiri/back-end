<?php
    function cleanInput($input)
    {
        $input = trim($input);
        $input = stripslashes($input);
        $input = htmlspecialchars($input);
        return $input;
    }
    if ($_SERVER["REQUEST_METHOD"]==="POST")
    {
        if (isset($_POST["ajouter"]))
        {
            $email = $_POST["email"];
            try {
                $pdo = require_once "connnection.php";
            $sql = 'INSERT INTO users(email) VALUES(:email);';
            $stm = $pdo->prepare($sql);
            $stm->bindParam(":email",$email);
            $stm->execute();
            $pdo=null;
            $msg = "L'utilisateur est ajoute avec success";
            }
            catch (PDOException $err) {
                $msg=$err->getMessage();
            }
            catch (\Throwable $th) {
                $msg=$th->getMessage();
            }
            
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h4>Ajouter Un Utilisateur</h4>
    <div>
        <p><?=isset($msg)?$msg:""?></p>
    </div>
    <form action="<?=htmlspecialchars($_SERVER["PHP_SELF"])?>" method="POST">
        <label for="">Email: </label>
        <input type="email" name="email"><br>
        <input type="submit" value="Ajouter" name="ajouter">
    </form>
</body>
</html>