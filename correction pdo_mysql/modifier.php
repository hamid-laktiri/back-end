<?php
    function cleanInput($input)
    {
        $input = trim($input);
        $input = stripslashes($input);
        $input = htmlspecialchars($input);
        return $input;
    }
    if ($_SERVER["REQUEST_METHOD"]==="POST")
    {
        if (isset($_POST["modifier"]))
        {
            $email = $_POST["email"];
            $id = $_POST["id"];
            try {
                $pdo = require_once "connnection.php";
            $sql = 'update   users set email = :email where id=:id;';
            $stm = $pdo->prepare($sql);
            $stm->bindParam(":id",$id);
            $stm->bindParam(":email",$email);
            $stm->execute();
            $pdo=null;
            $msg = "L'utilisateur est modifier avec success";
            }
            catch (PDOException $err) {
                $msg=$err->getMessage();
            }
            catch (\Throwable $th) {
                //throw $th;
            }
            
        }
        if (isset($_POST["rechercher"]))
        {
            $id = $_POST["id"];
            $pdo = require_once "connnection.php";
            $sql = "SELECT * FROM users WHERE id=:id";
            $stm = $pdo->prepare($sql);
            $stm->bindParam(":id",$id);
            $stm->execute();
            $result = $stm->fetch(PDO::FETCH_ASSOC); // get the mysqli result
          ;
            
                $email = $result["email"];
             
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h4>Modifier Un Utilisateur</h4>
    <div>
        <p><?=isset($msg)?$msg:""?></p>
    </div>
    <form action="<?=htmlspecialchars($_SERVER["PHP_SELF"])?>" method="POST">
        <label for="">id: </label>
        <input type="text" value="<?=isset($id)?$id:'';?>" name="id"><br>
        <label for="">Email: </label>
        <input type="email" value="<?=isset($email)?$email:'';?>" name="email"><br>
        <input type="submit" name="rechercher" value="rechercher">
        <input type="submit" value="modifier" name="modifier">
    </form>
</body>
</html>